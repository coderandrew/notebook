<?php

namespace Config;

class Config
{
    const PASSWORD_ALGO = PASSWORD_BCRYPT;
    const COOKIE_EXPIRY_TIME = 525600;
    const SITE_TITLE = 'Your App Title';
    const SITE_DESCRIPTION = 'Your App Description';

    public static function getDb()
    {
        $config = new \Doctrine\DBAL\Configuration();
        $params = [
            'dbname' => 'notebook',
            'user' => 'root',
            'password' => 'kmcakmca',
            'host' => 'localhost',
            'driver' => 'pdo_mysql'
        ];

        $conn = \Doctrine\DBAL\DriverManager::getConnection($params, $config);

        return $conn;
    }

}
