<?php
require 'vendor/autoload.php';
use Controllers\AuthController;
use Controllers\NotebookController;
use Config\Config;
use Models\Note;
use Models\Notebook;

$AuthController = new AuthController();

if (!empty($_COOKIE['auth_token'])) {
    $user = $AuthController->getUserByToken($_COOKIE['auth_token']);
    if(!$user){
        header("Location: index.php?logout");
    }
    $NotebookController = new \Controllers\NotebookController($user);
    $notebooks = $NotebookController->getNotebooksByUserId($user->user_id);
    if (isset($_POST['addnote'])) {

        $note = new Note($user);
        $note->title = $_POST['title'];
        $note->content = $_POST['content'];
        $note->notebook_id = $_POST['notebook_id'];
        $note->save();
        header('Location: ' . $_SERVER['REQUEST_URI']);
        exit();
    }

    if (isset($_POST['addnotebook'])) {
        $notebook = new Notebook($user);
        $notebook->title = $_POST['title'];
        $notebook->description = $_POST['description'];
        $notebook->save();
        header('Location: ' . $_SERVER['REQUEST_URI']);
        exit();
    }

    if (isset($_GET['view'])) {
        if (!empty($_GET['view'])) {
            $notebookController = new NotebookController($user);
            $notes = $notebookController->getNotesByNotebookId($_GET['view'], $user->user_id);
            $loader = new Twig_Loader_Filesystem('Views');
            $twig = new Twig_Environment($loader);
            echo $twig->render('Dashboard/view.html.twig', [
                'title' => Config::SITE_TITLE,
                'description' => Config::SITE_DESCRIPTION,
                'user' => $user,
                'notes' => $notes,
                'notebooks' => $notebookController->getNotebooksByUserId($user->user_id),
                'currentbook' => $notebookController->getNotebookById($_GET['view'])]);
        }
    } elseif (isset($_GET['delete'])) {
        if (!empty($_GET['delete'])) {
            $note = new Note($user);
            $note->note_id = $_GET['delete'];
            $notebook_id = $note->getNotebookIdByNoteId($_GET['delete']);
            $note->delete();
            header("Location: dashboard.php?view=$notebook_id");
            exit();
        }
    } elseif (isset($_GET['deletenotebook'])) {
        $notebook = new Notebook($user);
        $notebook->notebook_id = $_GET['deletenotebook'];
        $notebook->delete();
        header("Location: dashboard.php");
        exit();
    } else {
        $loader = new Twig_Loader_Filesystem('Views');
        $twig = new Twig_Environment($loader);
        echo $twig->render('Dashboard/dashboard.html.twig', [
            'title' => Config::SITE_TITLE,
            'description' => Config::SITE_DESCRIPTION,
            'user' => $user,
            'notebooks' => $notebooks]);
    }

} else {
    header('Location: index.php');
    exit();
}