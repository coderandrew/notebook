## Simple Self Hosted Notebook App

This application is simple to install and use for one or many people.

I do plan to maintain and keep this app going over time.

## Installation

This installation will require composer, so please ensure you have composer installed.

1) Upload the folder to your desired location.
2) cd into the directory and run composer install.
3) Modify Config/Config.php to match your database credentials.
4) Connect to your MYSQL server and issue the following commands. use database_name; source notebook.sql;

Once you've completed the steps above, you should be able to visit the folder on your web server and register an account.

I haven't written an installer yet, but it's coming along with the ability to edit a note.

Enjoy!
