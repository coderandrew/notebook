<?php

namespace Controllers;

use Config\Config;
use Models\User;

class AuthController
{
    private $user;
    private $errors;
    private $hasErrors = false;
    public $hasSession = false;

    public function auth($login, $password)
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('*')->from('users')->where('login = :login');
        $qb->setParameter(':login', $login);

        $res = $qb->execute()->fetch(5);

        if ($res) {
            if (password_verify($password, $res->password)) {
                $user = new User($res->login, $res->email, $res->registration_ts, $res->user_id);
                $this->user = $user;
                $this->getSessionToken();
                return $user;
            } else {
                $this->hasErrors = true;
                $this->setErrors(['Invalid Password.']);
            }
        } else {
            $this->hasErrors = true;
            $this->setErrors(['User not found.']);
        }


    }

    private function getSessionToken()
    {
        $timenow = date('Y-m-d H:i:s');
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('session_token, session_end')->from('session_tokens')->where('user_id = :user_id AND session_end > :timenow');
        $qb->setParameter(':user_id', $this->user->user_id);
        $qb->setParameter(':timenow', $timenow);

        $res = $qb->execute()->fetch(5);

        if ($res) {
            $this->user->hasSession = true;
            $this->user->setToken($res->session_token);
            setcookie('auth_token', $this->user->getToken(), time() + Config::COOKIE_EXPIRY_TIME, '/');
        } else {
            $this->generateToken();
            $this->getSessionToken();
        }
    }

    private function generateToken()
    {
        $this->clearOldTokens();
        $time = strtotime(date('Y-m-d H:i:s'));
        $sessionExpiry = date('Y-m-d H:i:s', strtotime("+" . Config::COOKIE_EXPIRY_TIME . "minutes", $time));
        $data = ['user_id' => $this->user->user_id, 'session_token' => sha1($this->user->getLogin() . microtime()), 'session_start' => date('Y-m-d H:i:s'), 'session_end' => $sessionExpiry];
        $db = Config::getDb();
        $db->insert('session_tokens', $data);
    }

    private function clearOldTokens()
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();
        $qb->delete('session_tokens')->where('user_id = :user_id');
        $qb->setParameter(':user_id', $this->user->user_id);
        $qb->execute();
    }

    private static function getUserByUserId($userId)
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('*')->from('users')->where('user_id = :user_id');
        $qb->setParameter(':user_id', $userId);

        $res = $qb->execute()->fetch(5);

        if ($res) {
            $user = new User($res->login, $res->email, $res->registration_ts, $res->user_id);
            return $user;
        } else {
            return false;
        }
    }

    public function getUserByToken($token)
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('*')->from('session_tokens')->where('session_token = :token');
        $qb->setParameter(':token', $token);

        $res = $qb->execute()->fetch(5);

        if (strtotime($res->session_end) <= strtotime(date('Y-m-d H:i:s'))) {
            return false;
        } else {
            return self::getUserByUserId($res->user_id);
        }
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function hasErrors()
    {
        return $this->hasErrors;
    }

    public function logout()
    {
        setcookie('auth_token', '', time() - Config::COOKIE_EXPIRY_TIME, '/');
    }

    public function registerUser($userData)
    {
        if (!$this->userExists($userData['login'], $userData['email'])) {
            $data['login'] = $userData['login'];
            $data['password'] = password_hash($userData['password'], Config::PASSWORD_ALGO);
            $data['email'] = $userData['email'];

            $db = Config::getDb();
            $db->insert('users', $data);
            return ['success' => true, 'msg' => 'Registration Successful'];
        } else {
            return ['success' => false, 'msg' => 'User already exists'];
        }
    }

    private function userExists($login, $email)
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('login')->from('users')->where('login = :login OR email = :email');
        $qb->setParameter(':login', $login);
        $qb->setParameter(':email', $email);
        $res = $qb->execute()->fetchAll();

        if (count($res) > 0) {
            return true;
        } else {
            return false;
        }
    }
}