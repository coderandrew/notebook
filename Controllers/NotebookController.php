<?php

namespace Controllers;


use Config\Config;
use Models\Notebook;

class NotebookController
{
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function getNotebooksByUserId($user_id)
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('*')->from('notebooks')->where('user_id = :user_id');
        $qb->setParameter(':user_id', $user_id);
        $res = $qb->execute()->fetchAll(5);

        $notebooks = [];

        foreach ($res as $notebook) {
            $return = new Notebook($this->user);
            $return->notebook_id = $notebook->notebook_id;
            $return->creation_date = $notebook->creation_date;
            $return->title = $notebook->title;
            $return->description = $notebook->description;
            $return->count = $return->getNoteCount($notebook->notebook_id);
            array_push($notebooks, $return);
        }
        return $notebooks;
    }

    public function getNotesByNotebookId($notebook_id, $user_id)
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('*')->from('notes')->where('notebook_id = :notebook_id AND user_id = :user_id')->orderBy('note_created', 'DESC');
        $qb->setParameter(':notebook_id', $notebook_id);
        $qb->setParameter(':user_id', $user_id);
        return $qb->execute()->fetchAll(5);
    }

    public function getNotebookById($notebook_id)
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('title,description')->from('notebooks')->where('notebook_id = :notebook_id');
        $qb->setParameter(':notebook_id', $notebook_id);

        $res = $qb->execute()->fetch(5);

        return $res;
    }

}