<?php

namespace Models;


class User
{
    private $login;
    private $token;
    public $user_id;
    public $email;
    public $joined;

    public function __construct($login, $email, $joined, $user_id)
    {
        $this->login = $login;
        $this->email = $email;
        $this->joined = $joined;
        $this->user_id = $user_id;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function getToken()
    {
        return $this->token;
    }

    public function setToken($token)
    {
        $this->token = $token;
    }

}