<?php
namespace Models;


use Config\Config;

class Note
{
    public $note_id;
    public $notebook_id;
    public $content;
    public $title;
    public $created;
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function save()
    {
        if ($this->verifyNotebookOwnership($this->user->user_id)) {
            $data['note_title'] = $this->title;
            $data['note_content'] = $this->content;
            $data['notebook_id'] = $this->notebook_id;
            $data['user_id'] = $this->user->user_id;
            $db = Config::getDb();
            $db->insert('notes', $data);
            return true;
        } else {
            return false;
        }


    }

    public function delete()
    {
        if ($this->verifyNoteOwnership()) {
            $db = Config::getDb();
            $qb = $db->createQueryBuilder();
            $qb->delete('notes')->where('note_id = :note_id AND user_id = :user_id');
            $qb->setParameter(':note_id', $this->note_id);
            $qb->setParameter(':user_id', $this->user->user_id);
            $qb->execute();
        }
    }

    private function verifyNotebookOwnership()
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();
        $qb->select('user_id')->from('notebooks')->where('notebook_id = :notebook_id');
        $qb->setParameter(':notebook_id', $this->notebook_id);

        $res = $qb->execute()->fetch(5);
        if ($res) {
            if ($res->user_id == $this->user->user_id) {
                return true;
            } else {
                return false;
            }
        }
    }

    private function verifyNoteOwnership()
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();
        $qb->select('user_id')->from('notes')->where('note_id = :note_id');
        $qb->setParameter(':note_id', $this->note_id);
        $res = $qb->execute()->fetch(5);

        if ($res) {
            if ($res->user_id == $this->user->user_id) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getNotebookIdByNoteId()
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('notebook_id')->from('notes')->where('note_id = :note_id');
        $qb->setParameter(':note_id', $this->note_id);
        $res = $qb->execute()->fetch(5);
        if ($res) {
            return $res->notebook_id;
        } else {
            return false;
        }
    }
}