<?php

namespace Models;

use Config\Config;

class Notebook
{
    public $title;
    public $description;
    public $creation_date;
    public $notebook_id;
    private $user;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function getNoteCount($notebook_id)
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();

        $qb->select('count(*)')->from('notes')->where('notebook_id = :notebook_id');
        $qb->setParameter(':notebook_id', $notebook_id);

        $res = $qb->execute()->fetch();

        return $res['count(*)'];
    }

    public function save()
    {
        $data['user_id'] = $this->user->user_id;
        $data['title'] = $this->title;
        $data['description'] = $this->description;
        $db = Config::getDb();
        $db->insert('notebooks', $data);
    }

    public function delete()
    {
        $db = Config::getDb();
        $qb = $db->createQueryBuilder();
        $qb->delete('notebooks')->where('notebook_id = :note_id AND user_id = :user_id');
        $qb->setParameter(':note_id', $this->notebook_id);
        $qb->setParameter(':user_id', $this->user->user_id);
        $qb->execute();
    }
}