function showAddNoteDialog() {
    if ($('#addnote').is(':visible')) {
        $('#addnote').hide();
    } else {
        $('#addnote').show();
    }
}

function showAddNotebook() {
    if ($('#addnotebook').is(':visible')) {
        $('#addnotebook').hide();
    } else {
        $('#addnotebook').show();
    }
}

function showAddNote(notebook_id) {
    if ($('#addnote').is(':visible')) {
        $('#addnote').hide();
    } else {
        $('#notebookselect').val(notebook_id);
        $('#addnote').show();
    }
}

function registerUser() {
        var data = $('#registration_form').serialize();
        $.ajax({
            method: 'POST',
            url: 'register.php',
            data: data,
            success: function (responsedata) {
                if (responsedata) {
                    $('#registrationDiv').hide();
                    if (responsedata.success == true) {
                        $('#login').before('<div hidden id="modal"><p>' + responsedata.msg + '</p></div>');
                        $('#modal').dialog();
                    } else {
                        $('#login').before('<div hidden id="modal"><p>' + responsedata.msg + '</p></div>');
                        $('#modal').dialog();
                    }

                }
            }
        });
}


function showRegistration() {
    if ($('#registrationDiv').is(':visible')) {
        $('#registrationDiv').hide();
    } else {
        $('#registrationDiv').show();
    }
}

