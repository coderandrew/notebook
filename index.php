<?php

require 'vendor/autoload.php';
use Controllers\AuthController;
Use Config\Config;

$login = false;
$password = false;
$user = false;


$AuthController = new AuthController();

if (isset($_GET['logout'])) {
    $AuthController->logout();
    header("Location: index.php");
    exit();
}

if (!empty($_COOKIE['auth_token'])) {
    $user = $AuthController->getUserByToken($_COOKIE['auth_token']);
}

if (isset($_POST['login'])) {
    $login = $_POST['login'];
}

if (isset($_POST['password'])) {
    $password = $_POST['password'];
}

if ($login && $password) {
    $user = $AuthController->auth($login, $password);
}

if ($user) {
    header('Location: dashboard.php');
} else {
    $loader = new Twig_Loader_Filesystem('Views');
    $twig = new Twig_Environment($loader);

    echo $twig->render('Login/login.html.twig', ['title' => Config::SITE_TITLE, 'description' => Config::SITE_DESCRIPTION]);
}




