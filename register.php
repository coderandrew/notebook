<?php
require 'vendor/autoload.php';
use Controllers\AuthController;

header('Content-Type: application/json');
if (isset($_POST['login']) && isset($_POST['password']) && isset($_POST['email'])) {
    if (empty($_POST['login']) || empty($_POST['password']) || empty($_POST['email'])) {
    echo json_encode(['success'=>false,'msg'=>'Form Incomplete! All fields are required.']);
    } else {
        $authController = new AuthController();
        echo json_encode($authController->registerUser($_POST));
    }
}